import React from 'react';

export const CompanyName = 'BERAS SEMESTA';
export const ProjectName = 'WEB APPLICATION';
export const MEMBER_PORTAL = 'http://member.berassemesta.com/';

export const DefaultLayout = React.lazy(() => import('../components/containers/DefaultLayout'));
