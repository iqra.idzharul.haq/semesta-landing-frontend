import React from 'react';

import RouterHome from '../modules/home/RouterHome';
import RouterCompany from '../modules/company/RouterCompany';
import RouterMarketingPlan from '../modules/marketingPlan/RouterMarketingPlan';

export const RouterContainer = [
  ...RouterHome, 
  ...RouterCompany,
  ...RouterMarketingPlan,
];

export const RouterNonContainer = [
  {
    path: '/404',
    exact: true,
    name: '404 Not Found',
    component: React.lazy(() => import('../modules/pages/views/Page404')),
  },
  {
    path: '/500',
    exact: true,
    name: '500 Internal Server Error',
    component: React.lazy(() => import('../modules/pages/views/Page500')),
  },
];
