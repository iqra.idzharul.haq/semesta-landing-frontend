import React, { Component, Fragment } from 'react';
import nojs from '../../../assets/img/nojs.png';
import logo from '../../../assets/img/brand/logo.png';
import ProductComponent from '../components/ProductComponent';
import ProductExcellence from '../components/ProductExcellence';
import PageTitle from '../../company/components/PageTitle';
import BenefitMember from '../components/BenefitMember';
import ContactForm from '../components/ContactForm';

class Home extends Component<any, any> {
  render() {
    return (
      <div>
          <div className="top-wrapper">
            <div className="container">
              <h1>BERAS SEMESTA</h1>
              <h2>BELANJA BERAS DAPAT UNTUNG</h2>
              <p>Beras Semesta adalah Perusahaan Direct Selling/Network Marketing <br />yang berbasis pada produk berkualitas dan konsumsi sehari hari dengan support system yang dapat diikuti oleh seluruh mitranya.</p>
            </div>
          </div>
          <BenefitMember title="KEUNTUNGAN MEMBER" history={this.props.history}/>
          <ProductExcellence title="KEUNGGULAN PRODUK"/>
          <ProductComponent/>
          <ContactForm title="HUBUNGI KAMI"/>
          {/* <div className="message-wrapper">
            <div className="container">
              <div className="heading">
                <h2>Apakah anda siap menjadi orang sukses?</h2>
                <h3>Ayo daftar menjadi member kami!</h3>
              </div>
              <span className="btn message">Daftar</span>
            </div>
          </div>
          <div id="form" className="contact-form ">
            <div className="container">
                <form>  
                    <p>Name</p>
                    <input placeholder="masukkan nama " type="text "/>
                    <p>Email</p>
                    <input placeholder="masukkan email " type="text "/>
                    <p>Messages</p>
                    <textarea placeholder="masukkan pesan " rows={5}></textarea>
                    <br/>
                    <input className="tombol " type="submit" value="Submit "/>
                </form>
                <img className="gambar-beside " src={nojs}/>
            </div>
          </div>
       */}
      </div>
    )
  }
}

export default Home;
