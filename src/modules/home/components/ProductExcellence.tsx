import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import { getProductExcellence } from "../../../utilities/api";

class ProductExcellence extends Component <any,any> {
  state ={
    left: [
      {
        "name": "Produk Eksklusif",
        "description": "Produk Beras semesta adalah merupakan Produk Ekslusif yang hanya dipasarkan melalui Beras semesta dan tidak diperjualbelikan selain oleh Member Beras semesta",
      },
      {
        "name": "Produk Konsumtif",
        "description": "Beras semesta memilih produk unggulan yang merupakan produk Beras yang dikonsumsi setiap hari dengan tingkat Repeat Order setiap hari",
      },
    ] as any[],
    right: [
      {
        "name": "Layanan Purna Jual",
        "description": "Beras semesta senantiasa menjaga kualitas produk yang di terima oleh Konsumen. Kami menerima retur, jika ditemukan produk-produk yang dibawah standar kelayakan",
      },
      {
        "name": "Harga Terjangkau",
        "description": "Perbandingan Harga Produk Beras semesta dengan Produk sejenis yang beredar di Indonesia tidak lebih dari dua kali",
      },
    ] as any[],
  }

  async componentDidMount(){
    const res = await getProductExcellence();
    const half = res.length/2;
    const rightData: any[] = [];
    const leftData: any[] = [];
    for(let i=0; i< res.length; i++){
      if(i<half){
        leftData.push(res[i]);
      }else{
        rightData.push(res[i]);
      }
    }
    this.setState({left: leftData, right: rightData})
  }

  render() {
    const { left, right } = this.state;
    const { title } = this.props;
    return(
      <div className="product-exellence pad-100-50">
        <Col>
          <Row className="mar-bot-15">
            <Col className=" text-align-center">
              <h1>{title}</h1>
            </Col>
          </Row>
          <Row>
            <Col className=" text-align-center">
              <p>Keunggulan dari produk-produk yang dipasarkan oleh Beras Semesta</p>
            </Col>
          </Row>
        </Col>
        <Row>
          <Col className="text-align-right">
          {
            left && left.map((item, index)=>{
              const {name, description} = item
              return(
                <div className="body-excel" key={name}>
                  <span className="fa fa-check fa-2x">&nbsp; {name}</span>
                  <p>{description}</p>
                </div>
              )
            })
          }
          </Col>
          <Col className="text-align-left">
          {
            right && right.map((item, index)=>{
              const {name, description} = item
              return(
                <div className="body-excel" key={name}>
                  <span className="fa fa-check fa-2x">&nbsp; {name}</span>
                  <p>{description}</p>
                </div>
              )
            })
          }
          </Col>
        </Row>
      </div>
    )
  }
}

export default ProductExcellence;