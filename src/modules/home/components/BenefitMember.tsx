import React, { Component } from "react";
import { Row, Col, Card, CardHeader, CardBody, CardFooter, Button } from "reactstrap";
import shoppingBonus from '../../../assets/icons/color/PNG/Online Shoping.png';
import levelBonus from '../../../assets/icons/color/PNG/Hockey stick growth.png';
import saleBonus from '../../../assets/icons/color/PNG/Product Management.png';

class BenefitMember extends Component <any,any> {
  state={
    data:[
      {
        name: 'Bonus Belanja',
        description: 'Dapatkan bonus poin setiap kali member dan down line nya melakukan pembelanjaan',
        url: shoppingBonus,
      },
      {
        name: 'Bonus Level',
        description: 'Dapatkan bonus ketika member berhasil mengisi slot member pada level tertentu',
        url: levelBonus,
      },
      {
        name: 'Penjualan Langsung',
        description: 'Dapatkan keuntungan atas penjualan produk langsung kepada konsumen non-member',
        url: saleBonus,
      },
    ]
  }
  render() {
    const { title } = this.props;
    const { data } = this.state;
    return (
      <div className="animated fadeIn pad-100">
        <Col>
          <Row className="mar-bot-15">
            <Col className=" text-align-center">
              <h1>{title}</h1>
            </Col>
          </Row>
          <Row className="mar-bot-50">
            <Col className=" text-align-center">
              <p>Keuntungan yang didapat ketika terdaftar sebagai member aktif beras semesta</p>
            </Col>
          </Row>
          <Row>
            {
              data && data.map((item)=>{
                return(
                  <Col className=" text-align-center">
                    <Card className="min-height-300 p-4">
                      <CardHeader className="bold">
                        <h3>{item.name}</h3>
                      </CardHeader>
                      <CardBody>
                      <div className="member-icon mar-bot-30">
                        <img src={item.url}/>
                      </div>
                      <p>{item.description}</p>
                      </CardBody>
                      <Button className="btn-default" onClick={()=>this.props.history.push('/marketing-plan')}>Detail</Button>
                    </Card>
                  </Col>
                )
              })
            }
          </Row>
        </Col>
      </div>

      // <div className="member">
      //   <div className="member-title">
      //     <h2>{title}</h2>
      //     <h3>Member Beras semesta akan mendapatkan Keuntungan atas Penjualan Langsung kepada Konsumen (non member). Keuntungan ini didapat dari selisih harga jual produk Member ke Konsumen (non member)</h3>
      //   </div>
      //   <div className="member-body">
          // <div className="member-item">
          //   <div className="member-icon">
          //     <img src={product}/>
          //     <p>Bonus Member</p>
          //   </div>
          //   <p className="text-contents">Dapatkan bonus dari member!</p>
          // </div>
          // <div className="member-item">
          //   <div className="member-icon">
          //     <img src={product}/>
          //     <p>Bonus Belanja</p>
          //   </div>
          //   <p className="text-contents">Dapatkan bonus belanja dari pembelanjaan pribadi anda!</p>
          // </div>
          // <div className="member-item">
          //   <div className="member-icon">
          //     <img src={product}/>
          //     <p>Bonus Level</p>
          //   </div>
          //   <p className="text-contents">Dapatkan bonus dari setiap member yang anda ajak!</p>
          // </div>
      //     <div className="clear"></div>
      //   </div>
      // </div>
    )
  }
}

export default BenefitMember;