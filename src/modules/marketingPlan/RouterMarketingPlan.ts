import React from 'react';

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  {
    path: '/marketing-plan',
    exact: true,
    name: 'Marketing Plan',
    component: React.lazy(() => import('./views/MarketingPlan')),
  },
];

export default routes;
