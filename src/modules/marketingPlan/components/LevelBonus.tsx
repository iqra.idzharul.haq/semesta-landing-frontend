import React, { Component } from "react";
import { Col, Row } from "reactstrap";
import shoppingBonus from '../../../assets/img/bonus-belanja.jpg';
import levelBonus from '../../../assets/img/bonus-level.jpg';

class LevelBonus extends Component<any, any> {
  render(){
    return (
      <div>
        <Row>
          <Col md="6" lg="6">
            <div className="marketing-page text-align-center">
              <div className="vision">  
                <h3>Bonus <span>BELANJA</span></h3>
                <p>Bonus poin belanja pada setiap level.</p>
                <img src={shoppingBonus}></img>
              </div>
            </div>
          </Col>
          <Col md="6" lg="6">
            <div className="marketing-page text-align-center">
              <div className="vision">  
                <h3>Bonus <span>LEVEL</span></h3>
                <p>Bonus poin level pada setiap level.</p>
                <img src={levelBonus}></img>
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          <Col xs="0" sm="0" md="3" lg="3">
          </Col>
          <Col md="6" lg="6">
            <div className="marketing-page text-align-center">
              <div className="vision">  
                <h3>Contoh <span>BONUS BELANJA</span></h3>
                <p>Contoh perhitungan bonus poin belanja.</p>
                <img src={shoppingBonus}></img>
              </div>
            </div>
          </Col>
          <Col xs="0" sm="0" md="3" lg="3">
          </Col>
        </Row>
      </div>
    )
  }
}

export default LevelBonus;