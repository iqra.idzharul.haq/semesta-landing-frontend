import { MEMBER_PORTAL } from "../../../config/App";

export default {
  items: [
    {
      name: "HOME",
      url: "/",
    },
    {
      name: "PERUSAHAAN",
      url: "/company",
    },
    {
      name: "KEMEMBERAN",
      children: [
        {
          name: "Penaftaran Member",
          url: MEMBER_PORTAL + "activate",
        },
        {
          name: "Marketing Plan",
          url: "/marketing-plan",
        },
      ],
    },
    {
      name: "PRODUK",
      children: [
        // {
        //   name: 'Profil Produk',
        //   url: '/product/profile',
        // },
        {
          name: "Varian Produk",
          url: "/product/variant",
        },
        {
          name: "Harga Produk",
          url: "/product/price",
        },
      ],
    },
    {
      name: "LOGIN",
      children: [
        {
          name: "Member Area",
          url: MEMBER_PORTAL,
        },
        {
          name: "Stokis Area",
          url: MEMBER_PORTAL + "transaction/create",
        },
      ],
    },
  ],
};
