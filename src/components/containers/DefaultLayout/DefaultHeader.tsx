import React, { Component } from 'react';
import {Nav} from 'reactstrap';
import PropTypes from 'prop-types';

import { AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import { CompanyName } from '../../../config';

import logo from '../../../assets/img/brand/logo-white.jpg';
import sygnet from '../../../assets/img/brand/logo-white.jpg';

const propTypes = {
  children: PropTypes.node, // eslint-disable-line
};

const defaultProps = {};

class DefaultHeader extends Component<any, any> {
  static propTypes: { children: PropTypes.Requireable<PropTypes.ReactNodeLike>; };
  static defaultProps: {};

  render() {
    // eslint-disable-next-line
    const { nav, openPage, children, ...attributes } = this.props;
    console.log(nav)

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <Nav className="nav-bar-menu" navbar></Nav>
        <AppNavbarBrand
          full={{
            src: logo, height: 50, alt: CompanyName,
          }}
          minimized={{
            src: sygnet, height: 50, alt: CompanyName,
          }}
          className="pointer"
          onClick={()=>openPage("/")}
        />
        <Nav className="ml-auto nav-bar-menu" navbar>
          {
            nav.items && nav.items.map((navItem: any)=>{
              return(
                <li className={navItem.children? "dropdown": ""}>
                  <p className={navItem.children? "dropbtn pointer": "pointer"} onClick={navItem.url? ()=>openPage(navItem.url) : ()=>{}}>{navItem.name}</p>
                  {
                    navItem.children && <div className="dropdown-content">
                    {
                      navItem.children && navItem.children.map((child: any)=>{
                        return(
                          <p className={child.children? "dropbtn pointer": "pointer"} onClick={child.url? ()=>openPage(child.url) : ()=>{}}>{child.name}</p>
                        )
                      })
                    }
                    </div>
                  }
                </li>
              )
            })
          }
        </Nav>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
