import React, { Component, Suspense } from 'react';
import * as RouterDom from 'react-router-dom';
import { Container } from 'reactstrap';

import {
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  // AppSidebarMinimizer,
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from './_nav';
// routes config
import { RouterContainer } from '../../../config';
import { MEMBER_PORTAL } from '../../../config/App';

const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component<any, any> {
  state={
    alert,
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  openPage(url: string){
    if(url.search(MEMBER_PORTAL) != -1){
      // window.open(url);
      window.location.assign(url);
    }else{
      this.props.history.push(url);
    }
  }

  render() {
    let navData = null;
    navData = navigation;
    const {alert} = this.state;

    return (
      <div className="app">
        <AppHeader fixed>
          <Suspense fallback={this.loading()}>
            <DefaultHeader nav={navData} openPage={(url:string)=>this.openPage(url)}/>
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed>
            <Suspense fallback={this.loading()}>          
              <AppSidebarNav navConfig={navData} {...this.props} router={RouterDom} />
            </Suspense>
            <AppSidebarFooter />
            {/* <AppSidebarMinimizer></AppSidebarMinimizer> */}
          </AppSidebar>
          <main className="main">
            {alert}
            <Container fluid>
              <Suspense fallback={this.loading()}>
                <RouterDom.Switch>
                  {RouterContainer.map((route, idx) => (route.component ? (
                    <RouterDom.Route
                      key={idx} // eslint-disable-line
                      path={route.path}
                      exact={route.exact}
                      name={route.name}
                      render={(props: any) => (
                        <route.component {...props} />
                      )}
                    />
                  ) : (null)))}
                  <RouterDom.Redirect from="/" to="/home" />
                </RouterDom.Switch>
              </Suspense>
            </Container>
          </main>
        </div>
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;
